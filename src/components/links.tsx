import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { LoginState } from "../store/store";

//Need to make this look not like garbage, but the logic is there

export default function Links(){

    const loginState:LoginState = {username:useSelector((state:LoginState) => state.username)};

    const tableStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    };

    return <>
        <div style={tableStyle}>
        {loginState.username ?<> 
        <Link to={"/signin"}>Sign Out </Link>&emsp;
        <Link to={"/create"}>Messages </Link>&emsp;
        <Link to={"/view"}>View Weddings</Link>
        </> : <>
        <Link to={"/signin"}>Sign In </Link>
        </>}
        </div>
    </>
}