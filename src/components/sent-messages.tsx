import axios from "axios";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { LoginState } from "../store/store";

export default function SentMessages(){
    const loginState:LoginState = {username:useSelector((state:LoginState) => state.username)};

    const [messageTable, updateTable] = useState(null);
    
    function formatTime(timestamp:string):string{
        const day = new Date(parseInt(timestamp));
        return day.toDateString();
    }

    async function getMessages(){
        const messageURL = 'https://messages-dot-project-1-revature-larson.ue.r.appspot.com/messages?sender='
        const result:any = await axios.get(messageURL + loginState.username);
        const messages:any[] = result.data;
        const messageRows:any = messages.map(m => <tr>
            <td>{m.recipient}</td>
            <td>{formatTime(m.timestamp)}</td>
            <td>{m.note}</td>
            </tr>)
        updateTable(messageRows);
    }
    
    useEffect(()=>{
        getMessages()
    }, [])

    return <>
        <h2>Sent Messages</h2>
        <table>
            <thead>
                <tr>
                <th>Sent To</th>
                <th>Time Sent</th>
                <th>Note</th>
                </tr>
            </thead>
            <tbody>
                {messageTable}
            </tbody>
        </table>
    </>
}