import axios from "axios";
import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { LoginState } from "../store/store";
import SingleWedding from "./view-single-wedding";


export default function  ViewWeddingPage(){

    const loginState:LoginState = {username:useSelector((state:LoginState) => state.username)};
    const API_IP = '34.74.202.196'
    const API_PORT = '3000'

    const dateRef:any = useRef(null);
    const locRef:any = useRef(null);
    const nameRef:any = useRef(null);
    const budgetRef:any = useRef(null);
    const reasonRef:any = useRef(null);
    const amountRef:any = useRef(null);
    const dateUpdate:any = useRef(null);
    const locUpdate:any = useRef(null);
    const nameUpdate:any = useRef(null);
    const budgetUpdate:any = useRef(null);

    const [tab, updateTab] = useState();
    const [weddingState, updateWeddingState] = useState(0);
    const [expenseTab, updateExpenseTab] = useState();

    async function deleteWedding(weddingId:number, updateTab:any){
        console.log(`Delete wedding ${weddingId}`);
        const deleted:any = await axios.delete(`http://${API_IP}:${API_PORT}/weddings/${weddingId}`);
        console.log(deleted);
        getTable(updateTab);
    }

    async function detailWedding(weddingId:number, updateTab:any){
        updateWeddingState(weddingId);
        getExpenseTable(weddingId, updateExpenseTab);
    }

    async function getTable(updateTab:any){
        const result:any = await axios.get(`http://${API_IP}:${API_PORT}/weddings`);
        const weddings:any[] = result.data;
        //console.log(weddings);
        const weddingRows = weddings.map(w => <tr key={w.weddingId}><td>{w.date}</td><td>{w.location}</td><td>{w.name}</td><td>{w.budget}</td><td>{w.weddingId}</td>
        <td><button onClick={()=>{
            detailWedding(w.weddingId, updateTab);
        }}>Details</button></td>
        <td><button onClick={()=>{
            deleteWedding(w.weddingId, updateTab);
        }}>Delete</button></td>
        </tr>)
        updateTab(weddingRows);
    }

    async function createExpense(){
        const fs = require('fs').promises;
        const reason = reasonRef.current.value;
        const amount = parseInt(amountRef.current.value);
        const result:any = await axios.post(`http://${API_IP}:${API_PORT}/expenses`, {
            expenseId:0,
            reason: reason,
            amount: amount,
            wId: weddingState
        });
        getExpenseTable(weddingState, updateExpenseTab)
    }

    useEffect(() =>{
        getTable(updateTab);
    }, []);


    async function submitWedding(){
        const date = dateRef.current.value;
        const location = locRef.current.value;
        const name = nameRef.current.value;
        const budget = parseInt(budgetRef.current.value)
        const result:any = await axios.post(`http://${API_IP}:${API_PORT}/weddings`, {
            weddingId:0,
            date: date,
            location: location,
            name: name,
            budget: budget
        });
        console.log(result);
        getTable(updateTab);
    }

    async function updateWedding(){
        const date = dateUpdate.current.value;
        const location = locUpdate.current.value;
        const name = nameUpdate.current.value;
        const budget = parseInt(budgetUpdate.current.value)
        const result:any = await axios.put(`http://${API_IP}:${API_PORT}/weddings/${weddingState}`, {
            weddingId:weddingState,
            date: date,
            location: location,
            name: name,
            budget: budget
        });
        console.log(result);
    }

    async function deleteExpense(expenseId:number, updateTab:any){
        console.log(`Delete expense ${expenseId}`);
        const deleted:any = await axios.delete(`http://${API_IP}:${API_PORT}/expenses/${expenseId}`);
        console.log(deleted);
        getExpenseTable(weddingState, updateTab);
    }

    async function getExpenseTable(weddingId:number, updateTab:any){
        const result:any = await axios.get(`http://${API_IP}:${API_PORT}/weddings/${weddingId}/expenses`);
        const expenses:any[] = result.data;
        const expenseRows = expenses.map(e => <tr key={e.expenseId}><td>{e.expenseId}</td><td>{e.reason}</td><td>{e.amount}</td>
        <td><button onClick={()=>{
            deleteExpense(e.expenseId, updateTab);
        }}>Delete</button></td>
        </tr>)
        console.log(expenses);
        updateTab(expenseRows);
    }

    const tableStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    };

    return <>{loginState.username && <div>
        <div style={tableStyle}>
        <label htmlFor="Date">Date:</label>
        <input name="Date" type="Date" ref={dateRef}></input>&emsp;
        <label htmlFor="Location">Location:</label>
        <input name="Location" ref={locRef}></input>&emsp;
        <label htmlFor="Name">Name:</label>
        <input name="Name" ref={nameRef}></input>&emsp;
        <label htmlFor="Budget">Budget:</label>
        <input name="Budget" type="Number" ref={budgetRef}></input>
        </div>
        <div style={tableStyle}>
        <button onClick={submitWedding}>Submit Wedding</button>
        </div>
        <br></br>
        <div style={tableStyle}>
            <table>
                <thead>
                    <tr>
                    <th>Date</th>
                    <th>Location</th>
                    <th>Name</th>
                    <th>Budget</th>
                    <th>Wedding Id</th>
                    <th>Details</th>
                    <th>Delete Wedding</th>
                    </tr>
                </thead>
                <tbody>{tab}</tbody>
            </table>
        </div>
        <SingleWedding weddingId={weddingState} expenseTab={expenseTab}
            reasonRef={reasonRef} amountRef={amountRef}
            createExpense={createExpense} dateUpdate={dateUpdate}
            locUpdate={locUpdate} nameUpdate={nameUpdate}
            budgetUpdate={budgetUpdate} updateWedding={updateWedding}
            imageRef></SingleWedding>
        </div>}
        </>
}