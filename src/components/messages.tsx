import { useState } from "react";
import { useSelector } from "react-redux";
import { LoginState } from "../store/store";
import SentMessages from "./sent-messages";
import RecievedMessages from "./recieved-messages";
import ComposeMessage from "./compose-message";

export default function CreateWeddingPage(){

    const [sOn, sTurnOn] = useState(false);
    const [rOn, rTurnOn] = useState(false);
    const [cOn, cTurnOn] = useState(false);

    const loginState:LoginState = {username:useSelector((state:LoginState) => state.username)};
    
    function viewSent(){
        sTurnOn(true);
        rTurnOn(false);
        cTurnOn(false);
    }

    function viewRecieved(){
        rTurnOn(true);
        cTurnOn(false);
        sTurnOn(false);
    }

    function viewCompose(){
        cTurnOn(true);
        rTurnOn(false);
        sTurnOn(false);
    }

    const tableStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    };

    return <>{loginState.username && <><h1>Messages</h1>
    <div style={tableStyle}>
        <button onClick={viewCompose}>Compose Message</button>
        <button onClick={viewRecieved}>View Recieved</button>
        <button onClick={viewSent}>View Sent</button>
    </div>
    {rOn && <RecievedMessages/>}
    {sOn && <SentMessages/>}
    {cOn && <ComposeMessage/>}
    </>}</>


}