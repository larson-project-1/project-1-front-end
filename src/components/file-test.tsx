import { useRef } from "react";
import axios from 'axios'

export default function FileInput(){
    
    const fileString:string = "";

    async function fileChanged(e:any){
        e.preventDefault()
        const reader = new FileReader()

        reader.onload = async (e:any) => { 
            const text:any = (e.target.result)
            return text;
        };
        alert(e.target.files[0].name)
        const [name, extension] = e.target.files[0].name.split('.');
        const content = reader.readAsDataURL(e.target.files[0])
        const result = axios.post('https://us-central1-project-1-revature-larson.cloudfunctions.net/upload-file',{
            name:name,
            extension:extension,
            content:content
        });
        console.log(result);
    }

    function fileInput(){
        if (fileString === ""){
            return;
        }
    }

    return <>
        <h1>FILES !!</h1>
        <input type='file' onChange={(e) => fileChanged(e)}></input>
        <button onClick={fileInput}>CLICK ME</button>
    </>
}