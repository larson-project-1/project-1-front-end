import axios from "axios";
import { useRef } from "react";
import { useSelector } from "react-redux";
import { LoginState } from "../store/store";

export default function ComposeMessage(){
    
    const loginState:LoginState = {username:useSelector((state:LoginState) => state.username)};

    const recipientRef:any = useRef(null);
    const noteRef:any = useRef(null);

    async function sendMessage(){
        const messageURL = 'https://messages-dot-project-1-revature-larson.ue.r.appspot.com/messages?';
        const recipient = recipientRef.current.value;
        const note = noteRef.current.value;
        const result:any = await axios.post(messageURL, {
            recipient:recipient,
            note:note,
            sender:loginState.username
        });
    }

    return <>
        <h2>Compose Message</h2>
        <label htmlFor="Recipient">To:</label>
        <input name="Recipient" ref={recipientRef}></input>
        <br></br>
        <label htmlFor="Note">Note:</label>
        <input name="Note" ref={noteRef}></input>
        <br></br>
        <button onClick={sendMessage}>Submit Message</button>
    </>
}