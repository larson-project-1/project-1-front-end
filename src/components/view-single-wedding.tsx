export default function SingleWedding(props:any){
    const weddingId = parseInt(props.weddingId);

    const tableStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    };


    return <>{ weddingId !== 0 && <>
        <h3 style={tableStyle}>Wedding {props.weddingId}</h3>
        <h4 style={tableStyle}>Update Wedding</h4>

        <div style={tableStyle}>
        <label htmlFor="Date">Date: </label>
        <input name="Date" type="Date" ref={props.dateUpdate}></input>&emsp;
        <label htmlFor="Location">Location: </label>
        <input name="Location" ref={props.locUpdate}></input>&emsp;
        <label htmlFor="Name">Name: </label>
        <input name="Name" ref={props.nameUpdate}></input>&emsp;
        <label htmlFor="Budget">Budget: </label>
        <input name="Budget" type="Number" ref={props.budgetUpdate}></input>&emsp;
        <br></br>
        </div>
        <br/>
        <div style={tableStyle}>
        <button onClick={props.updateWedding}>Update Wedding</button>
        </div>
        <h4 style={tableStyle}>Create Expense</h4>
        <div style={tableStyle}>
        <label htmlFor="Reason">Reason:</label>
        <input name="Reason" ref={props.reasonRef}></input>&emsp;
        <label htmlFor="Amount">Amount:</label>
        <input name="Amount" type="Number" ref={props.amountRef}></input>&emsp;
        <label htmlFor="Image">Upload Image</label>
        <input name="Image" type="file"></input>&emsp;
        <br/>
        </div>
        <br/>
        <div style={tableStyle}>
        <button onClick={props.createExpense}>Submit Expense</button>
        </div>
        <h4 style={tableStyle}>Expenses</h4>
        <div style={tableStyle}>
        <table>
        <thead>
            <tr>
                <th>Expense Id</th><th>Reason</th><th>Amount</th><th>Delete</th>
            </tr>
        </thead>
        <tbody>{props.expenseTab}</tbody>
    </table>
    </div>
    </>}</>
}