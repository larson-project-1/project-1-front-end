import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux"
import { LoginState } from "../store/store"
import axios from 'axios'

export default function LogInPage(){

    //const storeDays:DayState = {days:useSelector((state:DayState)=>state.days)};
    
    const usernameRef = useRef<any>(null);
    const passwordRef = useRef<any>(null);

    //should probably find a cleaner way to do this
    const loginState:LoginState = {
        username:useSelector((state:LoginState) => state.username)
    };

    const dispatch = useDispatch()

    async function login(){
        const usernameVal:string = usernameRef.current.value;
        const passwordVal:string = passwordRef.current.value;
        const loginURL = `https://login-dot-project-1-revature-larson.ue.r.appspot.com/users/login`
        //TODO VALIDATE USERNAME AND PASSWORD
        try{
            const result = await axios.patch(loginURL, {
                email:usernameVal,
                password:passwordVal
            });
            dispatch({type:"login", username:usernameVal});
        }catch{
            alert("Error: Username or Password Invalid");
        }
    }

    function logout(){
        dispatch({type:"logout", username:""});
    }

    const tableStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    };

    const logInTable =  <>
                        <div style={tableStyle}>
                            <input type="email" ref={usernameRef}></input>&emsp;
                            <input type="password" ref={passwordRef}></input>
                        </div>
                        <div style={tableStyle}>
                        <button onClick={login}>Log In!</button>
                        </div></>

    const logoutButton = <><div style={tableStyle}><button onClick={logout}>Log Out!</button></div></>

    return <div>
        {loginState.username ? logoutButton : logInTable}
    </div>
}