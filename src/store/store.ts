import { createStore, Reducer } from "redux";

export interface LoginState{
    username:string;
}

const reducer:Reducer<LoginState, any> = (loginState:LoginState  = {username:""}, message:any) =>{
    if(message.type === "login"){
        const newLoginState:LoginState = loginState
        newLoginState.username = message.username;// update state with new username
        return newLoginState;
    }
    if(message.type === "logout"){
        const newLoginState:LoginState = loginState
        newLoginState.username = "";// update state with new username
        return newLoginState;
    }
    return loginState;
}

const store = createStore(reducer); // we have created out store

export default store;