import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/header';
import { BrowserRouter as Router, Route } from "react-router-dom";
import store from './store/store';
import { Provider } from 'react-redux';
import ViewWeddingPage from './components/view-wedding-page';
import CreateWeddingPage from './components/messages';
import LogInPage from './components/log-in-page';
import Links from './components/links';
import { CssBaseline } from '@material-ui/core';
import ButtonAppBar from './components/app-bar';
import FileInput from './components/file-test';

ReactDOM.render(
  <React.StrictMode>
    <CssBaseline/>
    <Provider store={store}>
      <ButtonAppBar />
      <Header/>
      <Router>    
      <Links></Links>
    <Route path="/signin">
        <LogInPage></LogInPage>
    </Route>
    <Route path="/create">
      <CreateWeddingPage></CreateWeddingPage>
    </Route>
    <Route path="/view">
      <ViewWeddingPage></ViewWeddingPage>
    </Route>
    <Route path="/testfile">
      <FileInput></FileInput>
    </Route>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
