# Wedding Planner

## Project Description

The Wedding Planner allows employees at a buisness to create and view details of weddings via a REST API.  In this service, weddings can have attached expenses that can fall under a budget limit.  Along with this, employees can log in and send messages to each other.

Below is a link to the overview of what the Town Issue Review Board software does.

!(https://gitlab.com/AdamRanieri/node-gcp-batch/-/blob/main/Projects/Project2.md)

## Technologies Used

* App Egine
* Compute Engine
* Cloud Storage
* NodeJS
* Typescript
* React
* CloudSQL
* Cloud Datastore
* Cloud Functions

## Features

List of features ready
* An authorization service allowing employees to verify email and password to gain access to the rest of the submitted
* A messaging service allowing employees to send messages to each other
* A REST API allowing employees to create and edit weddings and expenses
* A Cloud Function that stores images associated with expenses in a Cloud Bucket

## Service Repositories

### Wedding API
https://gitlab.com/larson-project-1/project-1-api

### Messaging Service
https://gitlab.com/larson-project-1/project-1-messages

### Frontend
https://gitlab.com/larson-project-1/project-1-front-end

### File Upload Service
https://gitlab.com/larson-project-1/project-1-file-upload

### Login Service
https://gitlab.com/larson-project-1/project-1-login
